# get_era5



## requirements

- [a CDS account](https://cds.climate.copernicus.eu/#!/home)
- [the cdsapi python library](https://cds.climate.copernicus.eu/api-how-to)

## HOWTO

- modify the parameters in ```run_get_era5_sim-ready.py```

- the data are downloaded to a directory 'ERA5/' in the execution directory (see```get_era5_sim-ready_for-loop.py``` )

- request control parameters
- ```CHUNK_SIZE``` maximum request per user
- ```WAIT_SECONDS``` between each requests
- ```NB_MAX_LOOP_WAIT``` maximum loop iteration before break (stop the request loop)

- ERA5 control parameters
- ```deg``` resolution of the fixed grid in output
- ```year_list=[2024]``` list of years
- ```month_list=[i+1 for i in range(12)]``` list of months
- ```day_list=[16,17,18]``` list of days or ```day_list='all'```
- ```time_list = range(24)``` list of hours

## Fixed parameters (```get_era5_sim-ready_for-loop.py```)

- ```rootdatadir = './ERA5/'``` output directory
- ```type_mod=an``` mandatory for ERA5 outputs

- atmospheric parameters list
- ```param_levl_list = [129,130,133,246,248]```
- ```list_level = '1/to/137'```

- surface parameter list
- ```param_surf_list = [172,134,34,235,165,166,167,168,137,78,164]```

- specific MARS request
- ```'stream'  : 'oper'```               Denotes ERA5. Ensemble members are selected by 'enda'
- ```'format'  : 'netcdf'```             Output needs to be regular lat-lon, so only works in combination with 'grid'!
- ```grid'    : '%.2f/%.2f'%(deg,deg)``` Latitude/longitude. Default: spherical harmonics or reduced Gaussian grid


## Description of ECMWF/ERA5 parameters

```
# ~ LEVELS
# ~ Geopotential	              z	m2 s-2	    129			
# ~ Temperature	                  t	K	        130			
# ~ Specific humidity	          q	kg kg-1	    133			
# ~ Specific cloud liq wat     clwc kg kg-1	    246
# ~ Fraction of cloud cover      cc (0 - 1)     248

# ~ # SURFACE
# ~ Land-sea mask	            lsm	(0 - 1)	    172
# ~ Surface pressure             sp	Pa	        134		
# ~ Sea surface temperature	    sst	K	         34	
# ~ Skin temperature            skt K           235                         

# ~ 10 metre U wind component	10u	m s-1	    165			
# ~ 10 metre V wind component	10v	m s-1	    166			
# ~ 2 metre temperature	         2t	K	        167			
# ~ 2 metre dewpoint temperature 2d	K	        168		

# ~ Total column water vapour  tcwv	kg m-2	    137
# ~ Total column cloud liq wat tclw	kg m-2	    78

# ~ Total cloud cover	        tcc	(0 - 1)	    164

# ~ (forecast only)
# ~ Convective  rain rate        crr kg m-2 s-1 228218 
# ~ Large scale rain rate        crr kg m-2 s-1 228219
```
