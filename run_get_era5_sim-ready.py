#!/usr/bin/env python

import os,sys
import time as teatime

import pandas as pd
import subprocess

# ~ LEVELS
# ~ Geopotential	              z	m2 s-2	    129			
# ~ Temperature	                  t	K	        130			
# ~ Specific humidity	          q	kg kg-1	    133			
# ~ Specific cloud liq wat     clwc kg kg-1	    246
# ~ Fraction of cloud cover      cc (0 - 1)     248

# ~ # SURFACE
# ~ Land-sea mask	            lsm	(0 - 1)	    172
# ~ Surface pressure             sp	Pa	        134		
# ~ Sea surface temperature	    sst	K	         34	
# ~ Skin temperature            skt K           235                         

# ~ 10 metre U wind component	10u	m s-1	    165			
# ~ 10 metre V wind component	10v	m s-1	    166			
# ~ 2 metre temperature	         2t	K	        167			
# ~ 2 metre dewpoint temperature 2d	K	        168		

# ~ Total column water vapour  tcwv	kg m-2	    137
# ~ Total column cloud liq wat tclw	kg m-2	    78

# ~ Total cloud cover	        tcc	(0 - 1)	    164

# ~ (forecast only)
# ~ Convective  rain rate        crr kg m-2 s-1 228218 
# ~ Large scale rain rate        crr kg m-2 s-1 228219


# maximum request per user
CHUNK_SIZE = 10

WAIT_SECONDS     = 20
NB_MAX_LOOP_WAIT =  3

rootdatadir = './ERA5/'
type_mod = 'an'
deg = 0.1
suff_deg = '%dp%d'%(int(deg),100*(deg-int(deg)))

# ~ year_list  = [1992,1993,1994,1995,1996,1997,1998,1999]
# ~ year_list  = [2000,2001,2002,2003,2004,2005,2007,2008,2009]
# ~ year_list  = [2010,2011,2012,2013,2014,2015,2017,2018,2019]
#year_list = [1992,2000,2010,2020]
# ~ year_list = [2020]
# ~ year_list = [2012]
year_list = [2024]

# all months
# ~ month_list = [i+1 for i in range(12)]
#month_list = [i+1 for i in range(6)]
month_list = [4]

# ~ day_list   = [16,17,18]
day_list = 'all'
# ~ day_list = range(25,32)


# ~ time_list = [0]
# ~ time_list = [6,12]
# ~ time_list = range(11,25)
time_list = range(24)

# ================== get complete list of process
process_list = []
lvl_list     = []
for year in year_list:
    
    datadir = os.path.join(rootdatadir,'%d'%year)
    
    for month in month_list:
        if 'all' in day_list:
            day_list = [date.day for date in pd.date_range(start='%04d-%02d'%(year,month),
                                                             end='%04d-%02d'%(year,month+1),
                                                             closed='left')]
        for day in day_list:
            for time in time_list:
                process_list.append(['./get_era5_sim-ready_for-loop.py',
                                '--year', '%d'%year,
                                '--month','%d'%month,
                                '--day',  '%d'%day,
                                '--time', '%d'%time,    
                                '--deg',  '%.1f'%deg])    
                lvl_list.append(os.path.join(datadir,'%s_%d%02d%02dT%02d_%s_%s.nc'%('LVL',year,month,day,time,suff_deg,type_mod)))                                    

# ================== chunk
process_list_chunked = [process_list[i:i+CHUNK_SIZE] for i in range(0, len(process_list), CHUNK_SIZE)]
lvl_list_chunked     = [lvl_list[i:i+CHUNK_SIZE]     for i in range(0, len(lvl_list),     CHUNK_SIZE)]
     
# ================== run per chunk
for process_list,lvl_list in zip(process_list_chunked,lvl_list_chunked):
    # request CHUNCK processes
    for ip,process in enumerate(process_list):
        subprocess.Popen(process)    
    # wait for completion
    nbfiles = 0
    nb_wait = 0
    while nbfiles != len(lvl_list): 
        nbfiles = len([fname for fname in lvl_list if os.path.isfile(fname) ])
        teatime.sleep(WAIT_SECONDS)
        nb_wait += 1
        if nb_wait >= NB_MAX_LOOP_WAIT: break

