#!/usr/bin/env python

import os,sys
import copy 

import yaml
import argparse

import cdsapi




# Gestion des arguments/options
#-----------------------------------------------
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--year",  dest='year',  type=int, help='year')
parser.add_argument("--month", dest='month', type=int, help='month')
parser.add_argument("--day",   dest='day',   type=int, help='day')
parser.add_argument("--time",  dest='time',  type=int, help='time')
parser.add_argument("--deg",   dest='deg',   type=float, help='time')

args = parser.parse_args()
print(parser.prog.center(50,"-"))
print(args)

year  = args.year
month = args.month
day   = args.day
time  = args.time
deg   = args.deg
print(year,month,day,time)

# ~ import time as ttime
# ~ ttime.sleep(10)
# ~ print('.................... ok')
# ~ sys.exit(-1)

#-----------------------------------------------


# ~ LEVELS
# ~ Geopotential	              z	m2 s-2	    129			
# ~ Temperature	                  t	K	        130			
# ~ Specific humidity	          q	kg kg-1	    133			
# ~ Specific cloud liq wat     clwc kg kg-1	    246
# ~ Fraction of cloud cover      cc (0 - 1)     248

# ~ # SURFACE
# ~ Land-sea mask	            lsm	(0 - 1)	    172
# ~ Surface pressure             sp	Pa	        134		
# ~ Sea surface temperature	    sst	K	         34	
# ~ Skin temperature            skt K           235                         

# ~ 10 metre U wind component	10u	m s-1	    165			
# ~ 10 metre V wind component	10v	m s-1	    166			
# ~ 2 metre temperature	         2t	K	        167			
# ~ 2 metre dewpoint temperature 2d	K	        168		

# ~ Total column water vapour  tcwv	kg m-2	    137
# ~ Total column cloud liq wat tclw	kg m-2	    78

# ~ Total cloud cover	        tcc	(0 - 1)	    164

# ~ (forecast only)
# ~ Convective  rain rate        crr kg m-2 s-1 228218 
# ~ Large scale rain rate        crr kg m-2 s-1 228219


rootdatadir = './ERA5/'

type_mod = 'an'
# ~ type_mod = 'fc'

# ~ deg = 1.0
# ~ deg = 0.5
suff_deg = '%dp%d'%(int(deg),100*(deg-int(deg)))

# forecast only
stp_list = [3]


# ~ param_levl_list = [130]
param_levl_list = [129,130,133,246,248]
list_level = '1/to/137'

# ~ param_surf_list = [172]
param_surf_list = [172,134,34,235,165,166,167,168,137,78,164]
# ~ if type_mod == 'fc': surf_list.extend([228218,228219])

levl_param_list = ''.join(['%d/'%param for param in param_levl_list])[0:-1]
surf_param_list = ''.join(['%d/'%param for param in param_surf_list])[0:-1]

c = cdsapi.Client()

common_dict = \
{ # Requests follow MARS syntax    
 # Keywords 'expver' and 'class' can be dropped. They are obsolete
 # since their values are imposed by 'reanalysis-era5-complete'
    'stream'  : 'oper',                  # Denotes ERA5. Ensemble members are selected by 'enda'
    'format'  : 'netcdf',                # Output needs to be regular lat-lon, so only works in combination with 'grid'!
    'grid'    : '%.2f/%.2f'%(deg,deg),   # Latitude/longitude. Default: spherical harmonics or reduced Gaussian grid
}

spe_dict = {}
spe_dict['an'] = {}


spe_dict['fc'] = {}
step_list = ''.join(['%d/'%step for step in stp_list])[0:-1]
spe_dict['fc']['step'] = step_list


spe_dict['LVL'] = {}
# ~ Data on pressure levels, however, do not conserve vertically integrated quantities
spe_dict['LVL']['levtype']  = 'ml'
spe_dict['LVL']['levelist'] = list_level            # 1 is top level, 137 the lowest model level in ERA5. Use '/' to separate values.
spe_dict['LVL']['param']    = levl_param_list       # Full information at https://apps.ecmwf.int/codes/grib/param-db/

spe_dict['SFC'] = {}
spe_dict['SFC']['levtype']  = 'sfc'
spe_dict['SFC']['param']    = surf_param_list

for field in ['SFC','LVL']:
    
    # init
    request_dict = copy.deepcopy(common_dict)

    # output datadir
    datadir = os.path.join(rootdatadir,'%d'%year)
    if not os.path.exists(datadir): os.makedirs(datadir)

    # outname
    outname = os.path.join(datadir,'%s_%d%02d%02dT%02d_%s_%s.nc'%(field,year,month,day,time,suff_deg,type_mod))
    print(outname)

    # date
    date_dict = {'date'    : '%d-%02d-%02d'%(year,month,day)} # The hyphens can be omitted
    request_dict.update(date_dict)
    
    # hour/time
    # ~ time_dict = {"time": '00'}     
    # ~ time_dict = {"time": '00/to/23/by/1'} 
    time_dict = {"time": '%02d'%time}     # You can drop :00:00 and use MARS short-hand notation, instead of '00/06/12/18'} 
    request_dict.update(time_dict)

    # specifique analysis/forecast
    mod_dict = {'type'     : type_mod}
    request_dict.update(mod_dict)
    request_dict.update(spe_dict[type_mod])


    # specifique level ou surface
    request_dict.update(spe_dict[field])

    # request
    print(request_dict)
    try:
        c.retrieve('reanalysis-era5-complete', 
                        request_dict,
                        outname)         
    except:
        print("%s failed"%outname)



